import React from 'react';
import {NavLink} from "react-router-dom";
import './Post.css'


const  Post = props => {
        return(
            <div className="Post">
                <p>Created on: {props.date}</p>
                <h1>{props.title}</h1>
                <NavLink to={'/post/' + props.id}>Read more</NavLink>
            </div>
        )
};

export default Post;