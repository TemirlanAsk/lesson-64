import React, { Component, Fragment } from 'react';
import logo from './logo.svg';
import './App.css';
import {NavLink, Route, Switch} from "react-router-dom";
import PostsLists from "./containers/PostsLists/PostsLists";
import AddPost from "./containers/AddPost/AddPost";
import Contacts from "./containers/Contacts/Contacts";
import PostFull from "./containers/PostFull/PostFull";
import EditPost from "./containers/EditPost/EditPost";

class App extends Component {
  render() {
    return (
      <div>
          <Fragment>
              <nav className="App-nav">
                  <ul>
                      <li><NavLink to="/" exact>Home</NavLink></li>
                      <li><NavLink to="/add" exact >Add</NavLink></li>
                      <li><NavLink to="/about" >About</NavLink></li>
                      <li><NavLink to="/contacts" >Contacts</NavLink></li>
                  </ul>
              </nav>

              <Switch>
                  <Route path="/" exact component={PostsLists}/>
                  <Route path="/add" component={AddPost}/>
                  <Route path='/post/:id' exact component={PostFull} />
                  <Route path='/post/:id/edit' component={EditPost} />
                  {/*<Route path="/posts" component={About}/>*/}
                  {/*<Route path="/posts" component={Contacts}/>*/}
                  <Route render={() =>  <h1>Not Found</h1>}/>
              </Switch>
          </Fragment>
      </div>
    );
  }
}

export default App;
