import React, {Component} from 'react';
import './AddPost.css'
import axios from 'axios';

class  AddPost extends Component {
    state = {
        post:  {
            title: '',
            body: '',
            date: ''
        },
        loading: false
    };
    savePostHandler = e => {
        e.preventDefault();

        this.setState({loading: true});

        axios.post('/post.json', this.state.post).then(response => {
            this.setState({loading: false});
            this.props.history.replace('/');
        });
    };

    postValueChanged = e => {
        e.persist();
        this.setState(prevState => {
            return {
                post: {...prevState.post, [e.target.name]: e.target.value, date: Date()}
            }
        });
    };


    render(){
        return(
            <form className="AddPost">
                <h1>Add New post</h1>
                <input type="text" name="title" placeholder="Title" value={this.state.post.title} onChange={this.postValueChanged}/>
                <textarea name="body" placeholder="Description" value={this.state.post.body} onChange={this.postValueChanged}>
                </textarea>
                <button onClick={this.savePostHandler}>Save</button>
            </form>
        )
    }

};
export default AddPost;