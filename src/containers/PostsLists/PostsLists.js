import React, {Component, Fragment} from 'react';
import Post from "../../components/Post/Post";
import axios from 'axios';

class PostsLists extends Component {
    state = {
        posts: []
    };
    componentDidMount() {
        axios.get('/post.json').then((response) => {
            console.log(response);
            const posts = [];

            for(let key in response.data){
                posts.push({...response.data[key], id: key})
            }

            this.setState({posts})
        })
    }


    render() {
        return(
            <Fragment>
                <div>
                    {this.state.posts.map(post =>(
                        <Post
                            key={post.id} title={post.title} id={post.id} date={post.date}
                        />

                    ))}
                </div>
            </Fragment>
        )
    }
}
export default PostsLists;