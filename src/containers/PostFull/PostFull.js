import React, {Component, Fragment} from 'react';
import './PostFull.css'
import axios from 'axios';
import {NavLink, Route} from "react-router-dom";

class PostFull extends Component{
    state = {
        post: [],
        loadedPost: null
    };

        componentDidMount(){
           const id = this.props.match.params.id;

           axios.get(`/post/${id}.json`).then((response) =>{
               console.log(response.data);
               this.setState({loadedPost: response.data})
           })
        }

    deletePostHandler = (e, id) => {
        e.preventDefault()
        console.log(id, '------------')
        axios.delete(`/post/${id}.json`).then(() => {
            this.props.history.push('/');
        })
    };
        render(){
            if(this.state.loadedPost) {
                return (
                    <div className="PostFull">
                        <h1>{this.state.loadedPost.title}</h1>
                        <p>{this.state.loadedPost.body}</p>
                        <a href="#" onClick={(e) => this.deletePostHandler(e, this.props.match.params.id)}>Delete</a>
                        <NavLink to={'/post/' + this.props.match.params.id + '/edit'}>Edit</NavLink>
                    </div>
                )
            } else {
                return <p>Loading...</p>
            }
    }

}
export default PostFull;