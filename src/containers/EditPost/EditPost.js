import React, {Component} from 'react';
import axios from 'axios';

class EditPost extends Component {
    state = {
        post:  {
            title: '',
            body: ''
        },
        loading: false
    };

    componentDidMount(){
        const id = this.props.match.params.id;

        axios.get(`/post/${id}.json`).then((response) =>{
            console.log(response.data);
            this.setState({loadedPost: response.data, post: response.data})

        })
    }
    savePostHandler = e => {
        e.preventDefault();

        this.setState({loading: true});
        const id = this.props.match.params.id;

        axios.patch(`/post/${id}.json`, this.state.post).then(response => {
            this.setState({loading: false});
            this.props.history.replace('/');
        });
    };

    postValueChanged = e => {
        e.persist();
        this.setState(prevState => {
            return {
                post: {...prevState.post, [e.target.name]: e.target.value}
            }
        });
    };


    render(){
        return(
            <form className="AddPost">
                <h1>Edit  Post</h1>
                <input type="text" name="title" placeholder="Title" value={this.state.post.title} onChange={this.postValueChanged}/>
                <textarea name="body" placeholder="Description" value={this.state.post.body} onChange={this.postValueChanged}>
                </textarea>

                <button onClick={this.savePostHandler}>Save</button>
            </form>
        )
    }
}
export default EditPost;